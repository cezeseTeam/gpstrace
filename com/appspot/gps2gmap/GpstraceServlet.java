/**
 * <p>Title: Gps2gmapServlet.java</p>
 * <p>Description: 将GpsGate数据导入用户google map地图</p>
 * <p>Copyright: Copyright (c) 2010-2011 ceze</p>
 * @author ceze
 * @version 1.0
 * @date 2010-09-21
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * ==================================================================================================
 * ******Gpsgate（2.5.0+)数据上传协议
 * ref http://forum.gpsgate.com/topic.asp?TOPIC_ID=5637
 * GpsGate 2.5.0 build 207 (and later) can send GPS data over http. 
 * Any web server can receive and process the position data sent to it.
 *
 *URL format
 *The URL posted will have the following parameters:
	longitude=34.2333&latitude=23.2222&altitude=34.0&speed=30.3&heading=234.5&date=20070529&time=123445.234&username=myuser&pw=encryptedpassword
 *longitude: Decimal degrees (WGS84)
 *latitude: Decimal degrees (WGS84)
 *altitude: Above sea level in meters.
 *speed: Speed over ground in knots
 *heading: Heading in degrees. 0 is north, 90 is east, 180 is south, 270 is west.
 *date: UTC data in the format: YYYYMMDD
 *time: UTC time in the format: HHMMSS.sss
 *username: The username specified.
 *password: The password specified with a simple encryption. The encryption/decryption algorithm can be found here:
	http://forum.gpsgate.com/topic.asp?TOPIC_ID=5638
			
*======================================================================================================
 */

package com.appspot.gps2gmap;

import com.google.appengine.repackaged.com.google.protobuf.ServiceException;
import com.google.gdata.client.*;
import com.google.gdata.client.maps.*;
import com.google.gdata.data.*;
import com.google.gdata.data.maps.*;
import com.google.gdata.util.*;
import java.io.IOException;
import javax.servlet.http.*;
import java.util.logging.Logger;
import java.util.Map;
import java.util.Enumeration;
import java.lang.*;
import java.util.Date;
import java.util.TimeZone;



@SuppressWarnings("serial")
public class GpstraceServlet extends HttpServlet /*implements Runnable*/{
	
	private static final int 	ETestOK = 1;
	private static final int	EInitSuccess = 2; 
	private static final int 	EInitFails = -1;
	private static final int 	EUserPassWordErr = -2;
	
	
	private static final Logger log = Logger.getLogger(GpstraceServlet.class.getName());
	//url request 参数
	private static final String paraLon = "longitude";
	private static final String paraLat	= "latitude";
	private static final String paraAlt	= "altitude";
	private static final String paraSpeed = "speed";
	private static final String paraHead = "heading";
	private static final String paraDate = "date";
	private static final String paraTime = "time";
	private static final String paraUser = "username";
	private static final String paraPwd = "pw";
	
	private String sLon = "";
	private String sLat = "";
	private String sAlt = "";
	private String sSpeed = "";
	private String sHead = "";
	private String sDate = "";
	private String sTime = "";
	private String sUser = "";
	private String sPwd = "";
//	private String sDateTime = "";
	
	//Google Maps Service instance
	private MapsService gMapsService;
	
	/**
	 * HttpServlet 入口函数
	 *  Called by the server (via the service method) to allow a servlet to handle a GET request.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		   
		int ret = this.doInit(req);
		String respStr = "";
		switch(ret)
		{
		case ETestOK:
			respStr = "OK";
			break;
		case EInitSuccess:
			respStr = "Success";
			
			if(!(sLon.equals("0.00000") && sLat.equals("0.00000")))//WM可能传来0.00000/0.00000数据不做记录
			{
				try {
					GMapDataUtil.tracePoint(this.gMapsService, sUser, sPwd, sLon, sLat, sAlt,
							sHead, sSpeed, sDate, sTime);
				} catch (Exception e) {
					log.warning(e.toString());
				}
			}
			//TraceProxy tp = new TraceProxy(resp);
			//tp.googleClientLogin(sUser, sPwd);
			//tp.queryGMaps(sDate);
			//tp.doPostUrl("http://www.lvye.org", "data");
			//tp.doGetUrl("http://www.lvye.org");
			
			//run();
			//new Thread(this).start();
			break;
		case EInitFails:
			respStr = "User/PWD Wrong";
			break;
		case EUserPassWordErr:
			respStr = "Username/PWD NULL";
			break;
		}
		resp.getWriter().println(respStr);
	}//~end doGet


	/**
	 * HttpServlet入口函数
	 *  Called by the server (via the service method) to allow a servlet to handle a POST request.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws IOException{
		
		
		
	}
	
	
	/**
	 * doParseRequest
	 * @param req
	 * @throws AuthenticationException 
	 */
	private int doInit(HttpServletRequest req)
	{
		String debugStr = "";
			
		//request parameters
		sLon = req.getParameter(paraLon);
		sLat = req.getParameter(paraLat);
		sAlt = req.getParameter(paraAlt);
		sSpeed = req.getParameter(paraSpeed);
		sHead = req.getParameter(paraHead);
		sDate = req.getParameter(paraDate);
		sTime = req.getParameter(paraTime);
		sUser = req.getParameter(paraUser);
		sPwd = req.getParameter(paraPwd);
		
		//invert password	
		sPwd = invertString(sPwd);
		
		if(sUser == null || sPwd == null || sUser == "" || sPwd == "")
		{
			return EUserPassWordErr;
		}
		else
		{	
			try{
				gMapsService = new MapsService("vitular-gpstrace-1.1");
				gMapsService.setUserCredentials(sUser, sPwd);
			}
			catch(Exception e)
			{
				log.warning(e.toString());
				return EInitFails;
			}	
		}	
		//test connect case
		String test = req.getParameter("cmd");
		if(("test").equals(test))
		{
			return ETestOK;
		}
		
		//将速度由海里转为km
		if(sSpeed == "")
			sSpeed = "0";
		Float DSpeed =  Float.valueOf(sSpeed);
		float speed= (float)(1.854 * DSpeed.doubleValue());//换算关系
		sSpeed = String.valueOf(speed);
		DateTime d = DateTime.now();
		
		String tmpD = "";
		if(sDate != null && sDate != "")
		{
			tmpD += sDate.substring(0, 4) + "-" + sDate.substring(4,6) + "-" + sDate.substring(6,8);
		}
		String tmpT = "";
		if(sTime != null && sTime != "")
		{
			tmpT += sTime.substring(0,2) + ":" + sTime.substring(2,4) + ":" + sTime.substring(4,6);
		}
		DateTime uDT = DateTime.parseDateTime(tmpD + "T" +tmpT);	
		uDT.setTzShift(Integer.valueOf(480));
		String usDT = uDT.toUiString();
		int spaceIndex = usDT.indexOf(" ");
		sDate = usDT.substring(0, spaceIndex);
		sTime = usDT.substring(spaceIndex + 1);
		
		return EInitSuccess;
		
	}

	/**
	 * invert the encryption pwd.
	 * The password specified with a simple encryption. ref to http://forum.gpsgate.com/topic.asp?TOPIC_ID=5638
	 * @param strToInvert
	 * @return
	 */
	private String invertString(String strToInvert){
		if(strToInvert == null || strToInvert == "")
			return "";
	    StringBuilder builder = null;
	    if (strToInvert != null)
	    {
	        builder = new StringBuilder();
	        int iLength = strToInvert.length();
	        for (int iIndex = iLength - 1; iIndex >= 0; iIndex--)
	        {
	            char c = strToInvert.charAt(iIndex);
	            if (c >= '0' && c <= '9')
	            {
	                builder.append((char)(9 - (c - '0') + '0'));
	            }
	            else if (c >= 'a' && c <= 'z')
	            {
	                builder.append((char)(('z' - 'a') - (c - 'a') + 'A'));
	            }
	            else if (c >= 'A' && c <= 'Z')
	            {
	                builder.append((char)(('Z' - 'A') - (c - 'A') + 'a'));
	            }
	            else
	            {
	            	builder.append(c);
	            }
	        }
	    }
	    return builder != null ? builder.toString() : null;
	}//end invertString()
	
	

	/**
	 * implements Runnable.run() 
	 */
	public void run()
	{
		log.warning("run......................");
	}
}
