package com.appspot.gps2gmap;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Logger;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.Attribute;
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import java.util.Iterator; 


import javax.servlet.http.HttpServletResponse;

/**
 *  gmap data aip http操作代理
 * @author ceze
 *
 */
public class TraceProxy {
	
	private static final Logger log = Logger.getLogger(GMapDataUtil.class.getName());
	
	
	private HttpServletResponse m_Resp;
	//google 帐号登录auth
	private String m_LoginAuth;
	private String m_UserId;
	
	
	/**
	 * 构造
	 */
	public TraceProxy(HttpServletResponse resp)
	{
		this.m_Resp = resp;
		this.m_LoginAuth = "";
	}
	
	/**
	 * GET方式请求代理
	 * @param resp
	 * @param sUri
	 * @return
	 */
	public String doGetUrl(String sUri)
		throws MalformedURLException,IOException
	{
		URL url = new URL(sUri);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("GET");
        if(this.m_LoginAuth != "" && m_LoginAuth != null)
        {
        	//Authorization: GoogleLogin auth="authorization_token"
        	String value = "GoogleLogin auth=" + m_LoginAuth;
        	connection.setRequestProperty("Authorization", value);
        }


        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
        	
        	 BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
             String line;
             StringBuffer dataResp = new StringBuffer();
             while ((line = reader.readLine()) != null) {
                 dataResp.append(line);
             }
             //m_Resp.getWriter().print(dataResp.toString());
             return dataResp.toString();
             
        } else {
        	//m_Resp.getWriter().print("fail");
            return "";
        }
       
		/*
		URL url = new URL("http://www.google.com");
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
        String line;
        StringBuffer dataResp = new StringBuffer();
        
        while ((line = reader.readLine()) != null) {
            dataResp.append(line);
        	//resp.getWriter().print(line);
        }
        reader.close();
        return dataResp.toString();
        */   
	}
	
	/**
	 * POST方式请求代理
	 * @param resp
	 * @param sUri
	 * @param sData
	 * @return
	 */
	public void doPostUrl(String sUri, String sData, String sType)
		throws MalformedURLException,IOException
	{	
			URL url = new URL(sUri);
        	//String message = URLEncoder.encode(sData, "UTF-8");
        	
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", sType);
            
            if(this.m_LoginAuth != "" && m_LoginAuth != null)
            {
            	//Authorization: GoogleLogin auth="authorization_token"
            	String value = "GoogleLogin auth=" + m_LoginAuth;
            	connection.setRequestProperty("Authorization", value);
            }
            
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(sData);
            writer.close();
            
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
          
            }else{
            //	System.out.println("post ....");
            }
       /*    
            BufferedReader reader;
            String line;
            StringBuffer dataResp = new StringBuffer();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = reader.readLine()) != null) {
                dataResp.append(line);
            }
            System.out.println(dataResp.toString());
           */
            /*
            //m_Resp.getWriter().print(dataResp.toString());
             */
	}
	
	/**
	 * 
	 * @param sUri
	 * @param sData
	 * @param sType
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public void doPutUrl(String sUri, String sData, String sType)
		throws MalformedURLException,IOException
	{	
		URL url = new URL(sUri);
    	//String message = URLEncoder.encode(sData, "UTF-8");
    	
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);
        connection.setRequestMethod("PUT");
        connection.setRequestProperty("Content-Type", sType);
        connection.setRequestProperty(" X-HTTP-Method-Override", "PUT");
        if(this.m_LoginAuth != "" && m_LoginAuth != null)
        {
        	//Authorization: GoogleLogin auth="authorization_token"
        	String value = "GoogleLogin auth=" + m_LoginAuth;
        	connection.setRequestProperty("Authorization", value);
        }
        
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        writer.write(sData);
        writer.close();
        
        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
        //	System.out.print("ok....");
        }else{
        	
        }
      /*  
        BufferedReader reader;
        String line;
        StringBuffer dataResp = new StringBuffer();
        reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while ((line = reader.readLine()) != null) {
            dataResp.append(line);
        }
        System.out.println(dataResp.toString());
*/
}
                    
	
	/**
	 * 验证google帐户
	 * 参考 http://code.google.com/intl/zh-CN/apis/accounts/docs/AuthForInstalledApps.html
		 * Sample
		 * 
		 * POST /accounts/ClientLogin HTTP/1.0
			Content-type: application/x-www-form-urlencoded
			accountType=HOSTED_OR_GOOGLE&Email=jondoe@gmail.com&Passwd=north23AZ&service=cl&
   					source=Gulp-CalGulp-1.05
	 * @param sUser
	 * @param sPwd
	 * @return
	 */
	public boolean googleClientLogin(String sUser, String sPwd)
	{	
		final String  LOGIN_URL = "https://www.google.com/accounts/ClientLogin";
		this.m_UserId = sUser;
		boolean ret = false;
		String postData = "accountType=HOSTED_OR_GOOGLE&Email="+sUser+"&Passwd="+sPwd +
				"&service=local&source=ceze-gpstrace-1.0";
		
		try
		{
			URL loginURL = new URL(LOGIN_URL);
			BufferedReader reader;
            String line;
           // StringBuffer dataResp = new StringBuffer();
			
            //connect to host
			HttpURLConnection connection = (HttpURLConnection) loginURL.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
          //  connection.setRequestProperty("User-Agent", "local GData-Java/1");

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(postData);
            writer.close();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            	reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = reader.readLine()) != null) {
                	if(line.startsWith("Auth="))
                	{
                		reader.close();
                		//Get Auth string.
                		m_LoginAuth = line.substring(5);
                		return true;
                	}  
                }
                reader.close();
             //   System.out.print("login not auth found");
                
            }
            else if(connection.getResponseCode() == HttpURLConnection.HTTP_FORBIDDEN)
            {
            	//handle with  a CAPTCHA challenge
            }
            else
            {
            	System.out.println("login forbidden");
            	return false;
            }        
            
		}
		catch(MalformedURLException me)
		{
			System.out.print(me.toString());
		}
		catch(IOException e)
		{
			System.out.print(e.toString());
		}
		return ret;
	}//~ end ClientLogin()



/**
 * 查找map
 * @param sMapTitle
 * @return (mapId)
 * @deprecated
 */
public String queryGMaps(String sMapTitle)
{
	if(sMapTitle == null)
		return null;
	String sUrl = "";
	String respStr = "";
	String mapId = null;
	
	if(this.m_UserId != "")
		sUrl = "http://maps.google.com/maps/feeds/maps/" + m_UserId + "/owned";
	else 
		sUrl = "http://maps.google.com/maps/feeds/maps/full";
	
	try{
		respStr = doGetUrl(sUrl);
		if(respStr != "")
		{
			Document doc = DocumentHelper.parseText(respStr);
			Element root = doc.getRootElement();
			Iterator it =root.elements().iterator(); 
			   while(it.hasNext()) 
			   { 
				   Element info=(Element) it.next(); 
				    System.out.println(info.toString()); 
				    //System.out.println(info.elementText("index_code")); 
				    //System.out.println(info.elementText("content")); 
			   }
		}
	}
	catch(Exception e)
	{
		log.warning(e.toString());
	}
	
	return mapId;
}//end queryMap()

/**
 * 
 * @param sTitle
 * @return (mapId)
 * @deprecated
 */
private String createMap(String sTitle)
{
	String mapId = null;
	String sUrl = "";
	
	if(this.m_UserId != "")
		sUrl = "http://maps.google.com/maps/feeds/maps/" + m_UserId + "/owned";
	else 
		sUrl = "http://maps.google.com/maps/feeds/maps/full";
	
	try
	{
		
	}
	catch(Exception e)
	{
		//System.out.println("creatMap:"+e.toString());
		log.warning(e.toString());
	}
	
	return mapId;
}

/*
 *
 * POST http://maps.google.com/maps/feeds/features/userID/mapID/full
	Content-type: application/atom+xml
	Authorization: GoogleLogin auth="authorization_token"
 */
public void insertFeature(String sUrl, String sTitle, 
		String sFeatureContent, boolean isUpdate)
{
	if(sUrl == null || sUrl == "")
		return;
	
	String featureStr = "<atom:entry xmlns='http://www.opengis.net/kml/2.0' xmlns:atom='http://www.w3.org/2005/Atom'>"+
	   "<atom:title type='text'>" +
	   sTitle +
	   "</atom:title><atom:content type='application/vnd.google-earth.kml+xml'>" +
	   sFeatureContent + 
	   "</atom:content><atom:author><atom:name>ceze</atom:name></atom:author></atom:entry>"; //<atom:author><atom:name>cezese</atom:name></atom:author>
	
	
	String contentType = "application/atom+xml";
	
	try
	{
		if(isUpdate)
		{
			System.out.println("update true");
			doPutUrl(sUrl,featureStr, contentType);
		}
		else
		{
			doPostUrl(sUrl, featureStr, contentType);
		}
	}
	catch(Exception e)
	{
		log.warning(e.toString());
	}
}

}