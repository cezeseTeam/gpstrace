package com.appspot.gps2gmap;

/**
 * <p>Title: GMapDataUtil</p>
 * <p>Description: Google Maps Data API主要调用</p>
 * <p>Copyright: Copyright (c) 2010-2011 ceze</p>
 * @author ceze
 * @version 1.0
 * @date 2010-09-22
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.google.gdata.client.*;
import com.google.gdata.client.maps.*;
import com.google.gdata.data.*;
import com.google.gdata.data.maps.*;
import com.google.gdata.util.*;
import java.io.*;
import java.net.URL;
import java.util.logging.Logger;
import java.io.IOException;


/**
 * gmap data api 操作助手
 * @author ceze
 *
 */
public class GMapDataUtil {
		
	
	private static final String strGMapURL = "http://maps.google.com/maps/feeds/features/%s/%s/owned/"; //userid, mapid
	private static final String strGMapCreateURL = "http://maps.google.com/maps/feeds/maps/%s/full";
	private static final String LineTitle = "TraceLine";
	private static final String EndTitle = "P-lastest";
	private static final int	InterValCount = 10;//新记录点间隔时间
	private static final int 	IMiTraceNew = 10;//新路径间隔时间（分）
	
	 // Utility method to read KML from a file and create a string
	  private static String getKml(String filename) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    BufferedReader r = new BufferedReader(new FileReader(filename));
	    int c;
	    while ((c = r.read()) != -1) {
	      sb.append((char) c); 
	    } 
	    return sb.toString();
	  }
	  

	   
	  /**
	   * Get Feature
	   * @param myService
	   * @throws ServiceException
	   * @throws IOException
	   * @deprecated
	   */
	  public static void getFeature(MapsService myService)
	    throws ServiceException, IOException {

	    // Get a feature entry's #self URL (returned in the feature feed)
	        // Replace userID, mapID and featureID with appropriate values for your map's feature
	    final URL featureEntryUrl = new URL("http://maps.google.com/maps/feeds/features/userID/mapID/full/featureID");
	    
	        // Get the feature entry using the #self URL
	    FeatureEntry entry = myService.getEntry(featureEntryUrl, FeatureEntry.class);

	    // Print out the Feature title
	    System.out.println(entry.getTitle().getPlainText());

	    // Print out the KML
	    // Note that we use GData's getXml() method to print this out, as KML is XML
	    try {
	      XmlBlob kml = ((OtherContent) entry.getContent()).getXml();
	       
	    } catch(NullPointerException e) {
	      System.out.println("Null Pointer Exception");
	    }        
	  }//~end getFeature()
	  
	  /*
	   * 
	   */
	  public static FeatureEntry findEntryByTitle(FeatureFeed feeds, String aTitle)
	    throws ServiceException, IOException {

	    int count = feeds.getEntries().size();
	    for (int i = count; i  > 0; i--) {
	      FeatureEntry entry = feeds.getEntries().get(i-1);      
	      // Print the title of the feature , #self and #post links and the KML
	     // System.out.println("\nTitle: " + entry.getTitle().getPlainText());
	      if(entry.getTitle().getPlainText().equals(aTitle))
	      {
	    	  return entry;
	      }
	    }
	    return null;
	    
	  }//~end findEntryByTitle()
	  
	  
	  /**
	   * findMap
	   * @param mapService
	   * @throws ServiceException
	   * @throws IOException
	   */
	  public static MapEntry findMap(MapsService mapService, String mapTitle)
	    throws ServiceException, IOException {

	    // Request the default metafeed
	    final URL feedUrl = new URL("http://maps.google.com/maps/feeds/maps/default/owned");
	    MapFeed resultFeed = mapService.getFeed(feedUrl, MapFeed.class);

	    // Iterate through results 
	   
	    for (int i = 0; i < resultFeed.getEntries().size(); i++) {
	      MapEntry entry = resultFeed.getEntries().get(i);
	     
	      String title = entry.getTitle().getPlainText();
	      if(mapTitle.equals(title))
	      {
	    	  return entry;
	      }
	    }
	    return null;
	  }
	  
	  /**
	   * createMap
	   * @param mapService
	   * @return
	   * @throws ServiceException
	   * @throws IOException
	   */
	  public static MapEntry createMap(MapsService mapService, String userId, String mapTitle)
	    throws ServiceException, IOException {
		 
	    // Replace the following URL with your metafeed's POST (Edit) URL
		// Replace userID with appropriate values for your map
	    final URL editUrl = new URL(String.format(strGMapCreateURL, userId));
	    MapFeed resultFeed = mapService.getFeed(editUrl, MapFeed.class);
	    URL mapUrl = new URL(resultFeed.getEntryPostLink().getHref());
	    
	    // Create a MapEntry object
	    MapEntry mapEntry = new MapEntry();
	    mapEntry.setTitle(new PlainTextConstruct(mapTitle));
	    mapEntry.setSummary(new PlainTextConstruct("Summary:Created by gps2gmap.  [http://gps2gmap.appspot.com (Author ceze)]. "));
	   // mapEntry.getAuthors().add(new Person("vitular", null, "vitular@gmail.com"));
	    return mapService.insert(mapUrl, mapEntry);
	  }
	  
	 
	  
	  /**
	   * 
	   * @param mapService
	   * @param featureUrl
	   * @param KML
	   * @param fTitle
	   * @return
	   * @throws ServiceException
	   * @throws IOException
	   * @deprecated
	   */
	  public static FeatureEntry createFeature(MapsService mapService, final URL featureUrl, 
			  String KML, String fTitle)
      				throws ServiceException, IOException {
		  	// Create a blank FeatureEntry object
	 	    FeatureEntry featureEntry = new FeatureEntry(); 
	        try {
	        	// KML is simply XML so we'll use the XmlBlob object to store it
	        	XmlBlob kmlBlob = new XmlBlob();	
	        	kmlBlob.setFullText(KML);
	        	OtherContent ob = new OtherContent();
	        	ob.setXml(kmlBlob);
	        	ob.setMimeType(ContentType.APPLICATION_XML);
	        	
	        	featureEntry.setTitle(new PlainTextConstruct(fTitle));
	        	featureEntry.setContent(ob);
	        	//featureEntry.setKml(kmlBlob);
	        	//featureEntry.
	        } catch(NullPointerException e) {
	        	System.out.println("Error: " + e.getClass().getName());
	        }
	        // Insert the feature entry using the #post URL
	        return mapService.insert(featureUrl, featureEntry);
	  }//~end createFeature()


/**
 * 
 * @param mapService
 * @param userId
 * @param lon
 * @param Lat
 * @param alt
 * @param heading
 * @param date
 * @param time
 */
public static void tracePoint (MapsService mapService, String userId, String passWD,
		  String lon, String lat, String alt, String heading, String speed, String date, String time)
			throws ServiceException, IOException{
	  String mapTitle = "Tr-" + date;
	  MapEntry entry = findMap(mapService, mapTitle);
	  
	  boolean isInit = false;
	  if(entry == null)
	  {
		  entry = createMap(mapService, userId, mapTitle);
		  isInit = true;
	  }
	  String url = entry.getFeatureFeedUrl().toString();
	  FeatureFeed featureFeed = mapService.getFeed(entry.getFeatureFeedUrl(), FeatureFeed.class);
	  
	 
	//通过Http代理方式创建Feature
	//---------------------------路径Feature
	  FeatureEntry traceLineEntry = findEntryByTitle(featureFeed, LineTitle);
	  String featureContent = "";
	  TraceProxy tp = new TraceProxy(null);
	  boolean loginOk = tp.googleClientLogin(userId, passWD);
	    
	  //TraceLine
	  boolean newTrace = true;
	  if(traceLineEntry == null)
	  {
		  newTrace = true;
	  }
	  else
	  {
		  //超出一定时间，不要将轨迹和原来的连在一起2010/10/2
		  DateTime dt = traceLineEntry.getUpdated();
		  DateTime dtNow = DateTime.now();
		  long inval =  dtNow.getValue() - dt.getValue();
		  if((inval / (1000*60)) >= IMiTraceNew)//超过10分钟
		  {
			  //
			  newTrace = true;
			  //更新标题
			  String fUrl = traceLineEntry.getEditLink().getHref();
			  traceLineEntry.setTitle(new PlainTextConstruct("Track-"+time));
			  mapService.update(new URL(fUrl), traceLineEntry);
			  
		  }
		  else
		  {
			  newTrace = false;
		  }
	  }
	  
	  
	  if(newTrace == false)
	  {
		// Get a feature entry from its edit URL (returned in the feature feed)
			 //final URL featureEditUrl = new URL("http://maps.google.com/maps/feeds/features/userID/mapID/full/featureID");
		 XmlBlob lineBlob = ((OtherContent)traceLineEntry.getContent()).getXml();	 
		 String fullText = lineBlob.getBlob();
		 int offset = fullText.indexOf("</coordinates></LineString></Placemark>");
		 StringBuffer sb = new StringBuffer(fullText);
		 sb.insert(offset, " "+lon +","+lat+","+alt);
		 featureContent = sb.toString();		   
		 if(featureContent.length() >= 10*1024)//10k
		 {
			 newTrace = true;
			//更新标题
			  String fUrl = traceLineEntry.getEditLink().getHref();
			  traceLineEntry.setTitle(new PlainTextConstruct("Track-"+time));
			  mapService.update(new URL(fUrl), traceLineEntry);
		 }
	  }
	  
	  //update trace line
	  if(newTrace == true)
	  {
		  featureContent = "<Placemark><name>" + LineTitle + 
		     "</name><description/><LineString><extrude>1</extrude><tessellate>1</tessellate><coordinates>" +
		     	lon +"," + lat + "," + alt  +" "+ lon  +"2," + lat + "2,0"+
		      "</coordinates></LineString></Placemark>";
	  }
	  
	  //使用http代理
	  if(loginOk == true)
	  {
		tp.insertFeature(url, LineTitle, featureContent, false);
		if(newTrace == false)
		{
			String fUrl = traceLineEntry.getEditLink().getHref();
			mapService.delete(new URL(fUrl));//由于未知原因无法更新轨迹，删除原有轨迹再创建新轨迹
		}
	  }
	  
	  //----------------Trace Point---------------------
	  //终点Feature
	  FeatureEntry endPointEntry = findEntryByTitle(featureFeed, EndTitle);
	  
	  int traceNo = 1;
	  //使用End标记，目的是为了减少关键点的记录，免的地图“密密麻麻”
	  if(endPointEntry != null)
	  {
		  //删除end点
		String fUrl = endPointEntry.getEditLink().getHref();
		if(newTrace == false)
		{
			String content = endPointEntry.getKml().getFullText();
			int start = content.indexOf("[");
			int end = content.indexOf("]", start);
			String val = content.substring(start+1, end);
			traceNo = Integer.valueOf(val)+1;
			mapService.delete(new URL(fUrl));  
		}
		else
		{
			endPointEntry.setTitle(new PlainTextConstruct("P--"));
			mapService.update(new URL(fUrl), endPointEntry);
			isInit = true;
		}
	  }
	 
	  String title = "";
	  String des = "";
	  if(isInit || ((traceNo % InterValCount) == 0))//每30个点记录一个关键点(+ (line and end))
	  {
		  title = "P-" + time;	  	
		  des = "Point";
	  }
	  else
	  {
		  title = EndTitle; 
		  des = "Point[" + traceNo +"]"; 
		 
	  }
	  featureContent = "<Placemark><name>"+ title + 
	     "</name><description>"+ des + " Lon:" + lon + 
	     ", Lat:"+lat + ", Alt:" + alt + ". Speed:" + speed + "km/h,Degree:" + heading + 
	     " Time:"+time +"</description><Point><coordinates>" +
	     	lon +"," + lat + "," + alt +
	      "</coordinates></Point></Placemark>";	  
	  //create point
	  if(loginOk == true)
	  {
		  tp.insertFeature(url, title, featureContent, false);
	  }
	
	  
}
}
